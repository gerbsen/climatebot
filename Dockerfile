# we use a specific version to avoid surprises
# alpine versions did not work since there is stuff missing used by pip
FROM python:3.6.9
# copy our code to the app dir
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
RUN snips-nlu download de
RUN python3 src/train.py
ENTRYPOINT ["python3"]
CMD ["src/server.py"]
EXPOSE 9000