import sys
sys.path.append('../src')
sys.path.append('src')

import glob
import yaml
import unittest
import re
import interprete as queryInterpreter
from pathlib import Path
from pprint import pprint as pp

DATADIR = Path('data/intents')

def get_utterances():

    intents = glob.glob('data/intents/**/*.yaml', recursive=True)
    utterances = { }
    for intentFile in intents:
        with open(intentFile, 'r') as intentFileStream:
            intent = yaml.safe_load(intentFileStream)
            utterances[intent["name"]] = []
            for utterance in intent["utterances"]:
                utterances[intent["name"]].append(remove_variables_from_utterance(utterance))

    return utterances

def remove_variables_from_utterance(utterance):
    return re.sub('\[.*?\]', '', utterance).replace("(", "").replace(")", "")

class HandlerTests(unittest.TestCase):
    def test_utterances(self):

        print("Test utterances")
        allIntentsAndUtterances = get_utterances()
        nlu_engine = queryInterpreter.loadNLUEngine()
        correct = []
        incorrect = []

        for intent in allIntentsAndUtterances:
            for utterance in allIntentsAndUtterances[intent]:
                top_intent = queryInterpreter.parseText(engine=nlu_engine, text=utterance)
                if intent == top_intent["intent"]["intentName"]:
                    correct.append(utterance)
                else:
                    incorrect.append(f'''Should be "{intent}:" but is "{top_intent["intent"]["intentName"]}" for utterence "{utterance}"''')

        print("Incorrectly parsed questions:")
        pp(incorrect)
        print("Performance: " + str(round(len(correct) / (len(correct) + len(incorrect)), 4) * 100) + "%")



if __name__ == "__main__":
    unittest.main()