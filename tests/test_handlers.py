import sys
sys.path.append('../src')
sys.path.append('src')

import os
import glob
import yaml
import unittest
import traceback
import handler
from pathlib import Path
from collections import namedtuple

DATADIR = Path('data')
contents = namedtuple('yaml', 'data type fname')

def get_handlers():
    handler_dir = os.path.dirname(handler.__file__)
    handlers = []
    for file in glob.glob(handler_dir + '/*.py'):
        name = file.rsplit('/', 1)[-1].replace('.py', '')
        if not name.startswith('__'):
            handlers.append(name)

    return handlers

def compute_intents():
    data = []

    for intent_file_name in os.listdir(DATADIR / 'intents'):
        if not intent_file_name.endswith('.yaml'):
            continue

        with open(DATADIR / 'intents' / intent_file_name, 'r') as intent_file:
            content = yaml.load_all(intent_file.read(), Loader=yaml.FullLoader)

        for _content in content:
            data.append(contents(_content, _content['type'], intent_file_name))

    return data

class HandlerTests(unittest.TestCase):

    def test_handler_exists(self):
        """ Regardless of whether a handler works or not, does it exist? """
        # Get all intents
        intent_content = compute_intents()
        intents = [_content.data['name'] for _content in intent_content] + ['none']

        # Find handlers
        handlers = get_handlers();

        for intent in intents:
            self.assertIn(intent, handlers, msg=f'The intent "{intent}" does not have a handler.')

    def test_handler_works(self):
        """ Testing that _if_ a handler exists, does it work. Does not throw error if handler doesn't exist. """

        # Get all handlers
        handlers = [func for func in dir(handler) if not func.startswith('__')]

        # Dummy data
        data = {'input': 'Was ist die Klimakrise?',
                'intent': {'intentName': 'whatIsConcept', 'probability': 1.0},
                'slots': [
                    {'range': {'start': 12, 'end': 22},
                     'rawValue': 'Klimakrise',
                     'value': {'kind': 'Custom', 'value': 'Klimakrise'},
                     'entity': 'concept', 'slotName': 'concept'
                     }]}

        for intentname in handlers:
            _data = data.copy()
            _data['intent']['intentName'] = intentname

            fn = getattr(handler, intentname).handle

            try:
                res = fn(_data)
            except:     # TODO: define what errors to except here.
                traceback.print_exc()
                self.fail(msg=f"{intentname} handler doesn't work. See traceback below")

            self.assertIsNotNone(res.get('answer', None), msg=f"No answer returned from {intentname} handler.")


if __name__ == "__main__":
    os.chdir('..')
    # ht = HandlerTests()
    # ht.test_handler_works()

    unittest.main()