import json
import config.snips as snips
import config.entityMatching as entityMatching

def handle(data):

  countryEntity = snips.getFirstMatchingEntity(data, "country")
  if countryEntity is None:
    data["error"] = "no-entity-found-error: " + "country"
    return data

  country = entityMatching.getMatchingCountryObject(countryEntity)
  co2Data = country["data"]["co2EmmissionsByCountryTotal"]
  co2Value = co2Data["values"]["2017 - Fossil CO2-Emissions Per Capita(t CO2/cap/yr)"]

  # if year matches input
  data["answer"] = f'''
    Die CO<sub>2</sub> Emissionen pro Einwohner*in von {country["nameGerman"]} im Jahr 2017 lagen bei {co2Value} Tonnen CO<sub>2</sub> pro Einwohner pro Jahr.
  '''.strip()
  data["additionalInformation"] = {}
  data["additionalInformation"]["data"] = co2Data

  return data