import config.config as config
from pydash import _
from similarity.levenshtein import Levenshtein

"""
Loops through all available countries and returns the closest
match between the countries and the given input string.

:param countryName: the country name to search for
:return: the most similiar country according to levensthein similarity
"""
def getMatchingCountryObject(countryName):

  # loop through all countries to calch match
  for country in config.countries:
    country["similarity"] = Levenshtein().distance(country["nameGerman"], countryName)

  return _.sort(config.countries, key=lambda item: item["similarity"])[0]