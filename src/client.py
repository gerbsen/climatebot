import json
import requests
import sys

def to_test(query):
    '''
    This function needs to be added to the test suite and appropriately changed.
    :return:
    '''
    data    = { 'question': query }
    headers = { 'content-type': 'application/json'}

    r = requests.post('http://localhost:9000/query', data=json.dumps(data), headers=headers)
    if r.ok:
        print(r.text)
        print("The server responded without error. The answer still needs to be validated.")
    else:
        print(r)

query = sys.argv[1] if len(sys.argv) == 2 else "Was ist die Klimakrise?"
to_test(query)